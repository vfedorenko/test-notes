package by.vfedorenko.notes.presentation.notes.viewmodels

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.view.View
import android.widget.Toast
import by.vfedorenko.notes.businesslogic.data.NotesInteractor
import by.vfedorenko.notes.businesslogic.di.ActivityScope
import by.vfedorenko.notes.entities.Note
import by.vfedorenko.notes.presentation.App
import javax.inject.Inject

/**
 * @author Vlad Fedorenko <vfedo92@gmail.com> on 02.06.17.
 */
@ActivityScope
class NoteDetailsViewModel
@Inject constructor(private val interactor: NotesInteractor) : BaseObservable() {
    private var note = Note(id = null, title = App.EMPTY_STRING, body = App.EMPTY_STRING)

    @Bindable
    fun getTitle() = note.title
    fun setTitle(title: String) {
        note.title = title
    }

    @Bindable
    fun getBody() = note.body
    fun setBody(body: String) {
        note.body = body
    }

    fun onSaveClick(v: View) {
        if (note.title != App.EMPTY_STRING) {
            try {
                interactor.saveNote(note)
            } catch (e: Exception) {
                Toast.makeText(v.context, "Not Saved", Toast.LENGTH_SHORT).show()
            }

            Toast.makeText(v.context, "Saved", Toast.LENGTH_SHORT).show()
            // TODO: finish?
        } else {
            Toast.makeText(v.context, "Title is empty", Toast.LENGTH_SHORT).show()
        }
    }

    fun init(id: Long) {
        if (id != App.NO_ID) {
            interactor.getNote(id).observeForever {
                note = it ?: note
                notifyChange()
            }
        }
    }
}
