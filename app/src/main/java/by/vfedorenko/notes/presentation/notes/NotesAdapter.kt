package by.vfedorenko.notes.presentation.notes

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import by.vfedorenko.notes.BR
import by.vfedorenko.notes.R
import by.vfedorenko.notes.businesslogic.di.ActivityScope
import by.vfedorenko.notes.databinding.ItemNodeBinding
import by.vfedorenko.notes.entities.Note
import by.vfedorenko.notes.presentation.notes.viewmodels.NoteItemViewModel
import javax.inject.Inject

/**
 * @author Vlad Fedorenko <vfedo92@gmail.com> on 02.06.17.
 */
@ActivityScope
class NotesAdapter
@Inject constructor() : RecyclerView.Adapter<NotesAdapter.BindingHolder>() {
    class BindingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding: ItemNodeBinding = DataBindingUtil.bind(itemView)
    }

    private val data: MutableList<Note> = mutableListOf()

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int) =
            BindingHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_node, parent, false))

    override fun onBindViewHolder(holder: BindingHolder, position: Int) {
        val viewModel = NoteItemViewModel(data[position])

        holder.binding.setVariable(BR.viewModel, viewModel)
        holder.binding.executePendingBindings()
    }

    fun refresh(items: List<Note>) {
        data.clear()
        data.addAll(items)
        notifyDataSetChanged()
    }
}
