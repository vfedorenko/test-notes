package by.vfedorenko.notes.presentation.notes.activities

import android.databinding.DataBindingUtil
import android.os.Bundle
import by.vfedorenko.notes.R
import by.vfedorenko.notes.databinding.ActivityNotesBinding
import by.vfedorenko.notes.presentation.BaseActivity
import by.vfedorenko.notes.presentation.notes.viewmodels.NotesViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class NotesActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: NotesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityNotesBinding>(this, R.layout.activity_notes)
        binding.viewModel = viewModel

        setSupportActionBar(binding.toolbar)
    }
}
