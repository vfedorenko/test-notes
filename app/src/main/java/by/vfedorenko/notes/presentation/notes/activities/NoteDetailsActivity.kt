package by.vfedorenko.notes.presentation.notes.activities

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import by.vfedorenko.notes.R
import by.vfedorenko.notes.databinding.ActivityNoteDetailsBinding
import by.vfedorenko.notes.presentation.App
import by.vfedorenko.notes.presentation.BaseActivity
import by.vfedorenko.notes.presentation.notes.viewmodels.NoteDetailsViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class NoteDetailsActivity : BaseActivity() {
    companion object {
        private const val EXTRA_NOTE_ID = "noteId"

        fun createIntent(context: Context, id: Long = App.NO_ID) =
                Intent(context, NoteDetailsActivity::class.java).putExtra(EXTRA_NOTE_ID, id)
    }

    @Inject
    lateinit var viewModel: NoteDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityNoteDetailsBinding>(this, R.layout.activity_note_details)
        binding.viewModel = viewModel

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel.init(intent.getLongExtra(EXTRA_NOTE_ID, App.NO_ID))
    }
}
