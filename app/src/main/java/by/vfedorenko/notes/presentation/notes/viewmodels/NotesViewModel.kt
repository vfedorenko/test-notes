package by.vfedorenko.notes.presentation.notes.viewmodels

import android.arch.lifecycle.LiveData
import android.view.View
import by.vfedorenko.notes.businesslogic.data.NotesInteractor
import by.vfedorenko.notes.businesslogic.di.ActivityScope
import by.vfedorenko.notes.entities.Note
import by.vfedorenko.notes.presentation.notes.NotesAdapter
import by.vfedorenko.notes.presentation.notes.activities.NoteDetailsActivity
import javax.inject.Inject

/**
 * @author Vlad Fedorenko <vfedo92@gmail.com> on 01.06.17.
 */
@ActivityScope
class NotesViewModel
@Inject constructor(val adapter: NotesAdapter,
                    interactor: NotesInteractor) {

    private val notes: LiveData<List<Note>> = interactor.fetchNotes()

    init {
        notes.observeForever {
            adapter.refresh(it ?: arrayListOf())
        }
    }

    fun onAddClick(v: View) {
        v.context.startActivity(NoteDetailsActivity.createIntent(v.context))
    }
}
