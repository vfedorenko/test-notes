package by.vfedorenko.notes.presentation

import android.app.Activity
import android.app.Application
import by.vfedorenko.notes.businesslogic.di.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * @author Vlad Fedorenko <vfedo92@gmail.com> on 01.06.17.
 */
class App : Application(), HasActivityInjector {

    companion object {
        const val EMPTY_STRING = ""
        const val NO_ID = -1L
    }

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this)
    }
    override fun activityInjector() = dispatchingActivityInjector
}
