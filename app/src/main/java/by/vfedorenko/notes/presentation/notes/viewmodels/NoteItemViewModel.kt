package by.vfedorenko.notes.presentation.notes.viewmodels

import android.view.View
import by.vfedorenko.notes.entities.Note
import by.vfedorenko.notes.presentation.App
import by.vfedorenko.notes.presentation.notes.activities.NoteDetailsActivity

/**
 * @author Vlad Fedorenko <vfedo92@gmail.com> on 02.06.17.
 */
class NoteItemViewModel(private val note: Note) {
    fun getTitle() = note.title
    fun getBody() = note.body

    fun onItemClick(v: View) {
        v.context.startActivity(NoteDetailsActivity.createIntent(v.context, note.id ?: App.NO_ID))
    }
}
