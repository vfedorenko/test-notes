package by.vfedorenko.notes.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * @author Vlad Fedorenko <vfedo92@gmail.com> on 01.06.17.
 */
@Entity(tableName = "notes")
data class Note(
        @PrimaryKey(autoGenerate = true)
        var id: Long?,
        var title: String,
        var body: String)
