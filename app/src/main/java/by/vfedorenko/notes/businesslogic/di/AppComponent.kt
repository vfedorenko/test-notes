package by.vfedorenko.notes.businesslogic.di

import android.app.Application
import by.vfedorenko.notes.businesslogic.di.modules.ActivitiesModule
import by.vfedorenko.notes.businesslogic.di.modules.DataModule
import by.vfedorenko.notes.presentation.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * @author Vlad Fedorenko <vfedo92@gmail.com> on 01.06.17.
 */
@Singleton
@Component(modules = arrayOf(
        AndroidInjectionModule::class,
        ActivitiesModule::class,
        DataModule::class))
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    fun inject(app: App)
}
