package by.vfedorenko.notes.businesslogic.data

import by.vfedorenko.notes.entities.Note
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Vlad Fedorenko <vfedo92@gmail.com> on 02.06.17.
 */
@Singleton
class NotesInteractor
@Inject constructor(private val db: NotesDb, private val notesDao: NotesDao) {

    fun fetchNotes() = notesDao.getAll()

    fun getNote(id: Long) = notesDao.getById(id)

    fun saveNote(note: Note) {
        Thread(Runnable {
            db.beginTransaction()
            try {
                notesDao.insert(note)
                db.setTransactionSuccessful()
            } finally {
                db.endTransaction()
            }
        }).start()
    }
}
