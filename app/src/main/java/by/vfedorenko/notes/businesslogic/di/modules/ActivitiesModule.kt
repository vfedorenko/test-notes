package by.vfedorenko.notes.businesslogic.di.modules

import by.vfedorenko.notes.businesslogic.di.ActivityScope
import by.vfedorenko.notes.presentation.BaseActivity
import by.vfedorenko.notes.presentation.notes.activities.NoteDetailsActivity
import by.vfedorenko.notes.presentation.notes.activities.NotesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author Vlad Fedorenko <vfedo92@gmail.com> on 01.06.17.
 */
@Module
abstract class ActivitiesModule {
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun provideBaseActivity(): BaseActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun provideNotesActivity(): NotesActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun provideNoteDetailsActivity(): NoteDetailsActivity
}
