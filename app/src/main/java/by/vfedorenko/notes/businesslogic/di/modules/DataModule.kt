package by.vfedorenko.notes.businesslogic.di.modules

import android.app.Application
import android.arch.persistence.room.Room
import by.vfedorenko.notes.businesslogic.data.NotesDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Vlad Fedorenko <vfedo92@gmail.com> on 01.06.17.
 */
@Module
class DataModule {
    @Singleton
    @Provides
    fun provideDb(app: Application) = Room.databaseBuilder(app, NotesDb::class.java, "notes.db").build()

    @Singleton
    @Provides
    fun provideNotesDao(db: NotesDb) = db.notesDao()
}
