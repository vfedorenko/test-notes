package by.vfedorenko.notes.businesslogic.di

import javax.inject.Scope

/**
 * @author Vlad Fedorenko <vfedo92@gmail.com> on 01.06.17.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope
