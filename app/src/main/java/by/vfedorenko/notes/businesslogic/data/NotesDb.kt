package by.vfedorenko.notes.businesslogic.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import by.vfedorenko.notes.entities.Note

/**
 * @author Vlad Fedorenko <vfedo92@gmail.com> on 01.06.17.
 */
@Database(entities = arrayOf(Note::class), version = 1)
abstract class NotesDb : RoomDatabase() {
    abstract fun notesDao(): NotesDao
}
